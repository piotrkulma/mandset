package pl.piotrkulma.mandset;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import static pl.piotrkulma.mandset.Config.BOUNDARY;
import static pl.piotrkulma.mandset.Config.MAX_ITER;


/**
 * Created by Piotr Kulma on 25.06.2017.
 */
public final class MandUtils {
    private MandUtils() {
    }

    public static void drawJulia(GraphicsContext gc, Config config, double cx, double cy) {
        double iter;
        double tzx;

        double zx, zy;

        double x = -1, y;
        for(double px = config.getMinX(); px <= config.getMaxX(); px+=config.getStepX()) {
            x++;
            y=-1;
            for(double py = config.getMinY(); py<= config.getMaxY(); py+=config.getStepY()) {
                y++;
                iter = 0;

                zx = px;
                zy = py;

                do {
                    tzx = zx;
                    zx = zx * zx - zy * zy + cx;
                    zy = 2 * tzx * zy + cy;
                }while (iter++ <= MAX_ITER && (zx* zx - zy * zy) <= BOUNDARY);

                drawPoint(gc, iter, x, y);
            }
        }
    }

    public static void drawMand(GraphicsContext gc, Config config) {
        double iter;
        double zx, zy, tzx;

        double x = -1, y;
        for(double cx = config.getMinX(); cx <= config.getMaxX(); cx+=config.getStepX()) {
            x++;
            y=-1;
            for(double cy = config.getMinY(); cy<= config.getMaxY(); cy+=config.getStepY()) {
                y++;
                iter = 0;

                zx = 0;
                zy = 0;

                do {
                    tzx = zx;
                    zx = zx * zx - zy*zy + cx;
                    zy = 2 * tzx * zy + cy;
                }while (iter++ <= MAX_ITER && (zx* zx - zy * zy) <= BOUNDARY);

                drawPoint(gc, iter, x, y);
            }
        }
    }

    public static void drawPoint(GraphicsContext gc, double iter, double x, double y) {
        //Color color = Color.(0, 0, (int)iter);

        Color color = Color.hsb(Math.cos(iter/255d), Math.sin(iter/255d), iter/255d);
        gc.setStroke(color);
        gc.strokeLine(x, y, x + 1.0d, y + 1.0d);
    }
}
