package pl.piotrkulma.mandset;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import org.apache.log4j.Logger;

public class MandCanvasController {
    private final static Logger LOG = Logger.getLogger(MandCanvasController.class);

    private Config configMand;

    @FXML
    private Canvas mandCanvas;

    @FXML
    private Canvas juliaCanvas;

    @FXML
    protected void initialize() {
        initConfig();
    }

    @FXML
    protected void onCanvasScroll(ScrollEvent scrollEvent) {
        if(scrollEvent.getDeltaY() > 0) {
            scalePlus();
        } else {
            scaleMinus();
        }
    }

    @FXML
    protected void onCanvasMouseDragged(MouseEvent mouseEvent) {
        drawJulia(mouseEvent);
    }

    @FXML
    protected void onCanvasMouseClicked(MouseEvent mouseEvent) {
        drawJulia(mouseEvent);
    }

    private void drawJulia(MouseEvent mouseEvent) {
        double px, py;
        double mouseX = mouseEvent.getX();
        double mouseY = mouseEvent.getY();

        double halfStep = Config.MAX_STEPS / 2d;

        if(mouseX > halfStep) {
            mouseX = mouseX - halfStep;
            px = (mouseX / halfStep) * configMand.getMaxX();
        } else {
            px = configMand.getMinX() - ((mouseX / halfStep) * configMand.getMinX());
        }

        if(mouseY > halfStep) {
            mouseY = mouseY - halfStep;
            py = -1d * (mouseY / halfStep) * configMand.getMaxY();
        } else {
            py = configMand.getMinY() - ((mouseY / halfStep) * configMand.getMinY());
            py *=-1;
        }
        LOG.info(px + " " + py);
        GraphicsContext gc = juliaCanvas.getGraphicsContext2D();
        gc.clearRect(0, 0, juliaCanvas.getWidth(), juliaCanvas.getHeight());

        Config juliaConfig = new Config(-2d, 2d, -1d, 1d);
        MandUtils.drawJulia(gc, juliaConfig, px, py);

        LOG.info(px + " " + py);
    }

    @FXML
    protected void drawMandClick() {
        GraphicsContext gc = mandCanvas.getGraphicsContext2D();
        gc.clearRect(0, 0, mandCanvas.getWidth(), mandCanvas.getHeight());
        MandUtils.drawMand(gc, configMand);

        LOG.info(configMand);
    }

    @FXML
    private void scalePlus() {
        configMand.scalePlus(0.1d);
        drawMandClick();
    }

    @FXML
    private void scaleMinus() {
        configMand.scaleMinus(0.1d);
        drawMandClick();
    }

    @FXML
    private void moveUp() {
        configMand.moveY(0.1d);
        drawMandClick();
    }

    @FXML
    private void moveDown() {
        configMand.moveY(-0.1d);
        drawMandClick();
    }

    @FXML
    private void moveLeft() {
        configMand.moveX(-0.1d);
        drawMandClick();
    }

    @FXML
    private void moveRight() {
        configMand.moveX(0.1d);
        drawMandClick();
    }

    private void initConfig() {
        if(configMand == null) {
            configMand = new Config(-2d, 1d, -1d, 1d);
        }
    }
}
