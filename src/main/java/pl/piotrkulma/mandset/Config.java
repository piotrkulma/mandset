package pl.piotrkulma.mandset;

/**
 * Created by Piotr Kulma on 24.06.2017.
 */
public final class Config {
    public static final double MAX_ITER    = 253;

    public static final double BOUNDARY    = 4d;

    //how many points per each row
    public static final double MAX_STEPS   = 500d;

    private double minX;
    private double maxX;
    private double minY;
    private double maxY;

    private double rangeX;
    private double rangeY;

    private double stepX;
    private double stepY;

    public Config(double minX, double maxX, double minY, double maxY) {
        this.minX = minX;
        this.maxX = maxX;

        this.minY = minY;
        this.maxY = maxY;
        calculate();
    }

    public double getStepX() {
        return stepX;
    }

    public double getStepY() {
        return stepY;
    }

    public double getMinX() {
        return minX;
    }

    public double getMaxX() {
        return maxX;
    }

    public double getMinY() {
        return minY;
    }

    public double getMaxY() {
        return maxY;
    }

    public double getRangeX() {
        return rangeX;
    }

    public double getRangeY() {
        return rangeY;
    }

    public void scaleMinus(double ratio) {
        double valueX = ratio / rangeX;

        minX -= valueX;
        maxX += valueX;

        double valueY = ratio / rangeY;

        minY -= valueY;
        maxY += valueY;

        calculate();
    }

    public void scalePlus(double ratio) {
        double valueX = ratio / rangeX;

        minX += valueX;
        maxX -= valueX;

        double valueY = ratio / rangeY;

        minY += valueY;
        maxY -= valueY;

        calculate();
    }

    public void moveX(double value) {
        minX += value;
        maxX += value;

        calculate();
    }

    public void moveY(double value) {
        minY += value;
        maxY += value;

        calculate();
    }

    private void calculate() {
        rangeX = (maxX - minX);
        rangeY = (maxY - minY);

        stepX = rangeX / MAX_STEPS;
        stepY = rangeY / MAX_STEPS;
    }

    @Override
    public String toString() {
        return "Config{" +
                "minX=" + minX +
                ", maxX=" + maxX +
                ", minY=" + minY +
                ", maxY=" + maxY +
                ", rangeX=" + rangeX +
                ", rangeY=" + rangeY +
                ", stepX=" + stepX +
                ", stepY=" + stepY +
                '}';
    }
}
